#include <stdlib.h>
#include <stdio.h>

int	**pascalTr(int size)
{
  int	**ret;
  int	i;
  int	j;
  
  if (size <= 0)
    return (NULL);
  if (!(ret = malloc(sizeof(*ret) * size)))
    return (NULL);
  i = 0;
  while (i < size)
    {
      if (!(ret[i] = malloc(sizeof(**ret) * (i + 1))))
	return (NULL);
      j = 0;
      while (j < (i + 1))
	{
	  if (j == 0 || j == i)
	    ret[i][j] = 1;
	  else
	    ret[i][j] = ret[i - 1][j] + ret[i - 1][j - 1];
	    
	  ++j;
	}
      ++i;
    }
  return (ret);
}
