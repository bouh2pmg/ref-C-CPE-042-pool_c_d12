#include <stdio.h>

void	print_unique(int *tab, int size)
{
  int	i;
  int	j;
  int	p;
  
  i = 0;
  p = 0;
  while (i < size)
    {
      j = 0;
      while (j < size)
	{
	  if (i != j && tab[i] == tab[j])
	    {
	      break;
	    }
	  ++j;
	}
      if (j == size)
	{
	  if (p == 0)
	    {
	      printf("%d", tab[i]);
	      p = 1;
	    }
	  else
	    printf(",%d", tab[i]);
	}
	  ++i;
    }
  printf("\n");
}
